<?php

/**
 * @file
 */

use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Hook_uninstall function.
 */
function social_lms_quiz_uninstall() {

  // Array of all configuration.
  $configuration = [
    'core.entity_form_display.node.quiz.default',
    'core.entity_view_display.node.quiz.default',
    'core.entity_view_display.node.quiz.teaser',
    'field.field.node.quiz.body',
    'field.field.node.quiz.field_quiz_board_field',
    'field.field.node.quiz.field_quiz_chapter_field',
    'field.field.node.quiz.field_quiz_class_field',
    'field.field.node.quiz.field_quiz_cognitive_table_field',
    'field.field.node.quiz.field_quiz_concept_field',
    'field.field.node.quiz.field_quiz_duration',
    'field.field.node.quiz.field_quiz_instruction',
    'field.field.node.quiz.field_quiz_learning_objective',
    'field.field.node.quiz.field_quiz_quiz_type',
    'field.field.node.quiz.field_quiz_subject',
    'field.field.node.quiz.field_quiz_text_book',
    'field.storage.node.field_quiz_board_field',
    'field.storage.node.field_quiz_chapter_field',
    'field.storage.node.field_quiz_class_field',
    'field.storage.node.field_quiz_cognitive_table_field',
    'field.storage.node.field_quiz_concept_field',
    'field.storage.node.field_quiz_duration',
    'field.storage.node.field_quiz_instruction',
    'field.storage.node.field_quiz_learning_objective',
    'field.storage.node.field_quiz_quiz_type',
    'field.storage.node.field_quiz_subject',
    'field.storage.node.field_quiz_text_book',
    'node.type.quiz',
    'taxonomy.vocabulary.board_field',
    'taxonomy.vocabulary.chapter_field',
    'taxonomy.vocabulary.class_field',
    'taxonomy.vocabulary.cognitive_table_field',
    'taxonomy.vocabulary.concept_field',
    'taxonomy.vocabulary.learning_objective',
    'taxonomy.vocabulary.quiz_type',
    'taxonomy.vocabulary.subject',
    'taxonomy.vocabulary.text_book',
    'field.storage.node.field_quiz_question',
    'node.type.question',
    'field.field.node.quiz.field_quiz_question',
    'field.field.node.question.body',
    'core.entity_form_display.node.question.default',
    'core.entity_view_display.node.question.teaser',
    'core.entity_view_display.node.question.default',
    'core.entity_form_display.node.quiz.default',
    'core.entity_view_display.node.quiz.default',
    'field.storage.paragraph.field_question',
    'core.entity_view_mode.paragraph.preview',
    'paragraphs.paragraphs_type.quiz_question_para',
    'field.field.paragraph.quiz_question_para.field_question',
    'core.entity_view_display.paragraph.quiz_question_para.default',
    'core.entity_form_display.paragraph.quiz_question_para.default',
    'paragraphs.settings',
    'field.storage.node.field_quiz_question_paragraph',
    'field.field.node.quiz.field_quiz_question_paragraph',
    'field.storage.node.field_question_options',
    'field.field.node.question.field_question_options',
    'field.storage.paragraph.field_ld_description',
    'field.storage.paragraph.field_correct',
    'paragraphs.paragraphs_type.quiz_question_options',
    'field.field.paragraph.quiz_question_options.field_ld_description',
    'field.field.paragraph.quiz_question_options.field_correct',
    'field.storage.paragraph.field_o_learning_deficiency',
    'taxonomy.vocabulary.learning_deficiency',
    'field.field.paragraph.quiz_question_options.field_o_learning_deficiency',
    'field.storage.paragraph.field_remedial_measure',
    'field.storage.paragraph.field_option_value',
    'field.field.paragraph.quiz_question_options.field_remedial_measure',
    'field.field.paragraph.quiz_question_options.field_option_value',
    'core.entity_view_display.paragraph.quiz_question_options.default',
    'core.entity_form_display.paragraph.quiz_question_options.default',
    'field.storage.node.field_quiz_assignment_quiz',
    'node.type.quiz_assignment',
    'field.field.node.quiz_assignment.field_quiz_assignment_quiz',
    'field.storage.paragraph.field_start_date',
    'field.storage.paragraph.field_report_visibility',
    'field.storage.paragraph.field_end_date',
    'field.storage.node.field_quiz_assignment_section_de',
    'paragraphs.paragraphs_type.section_details',
    'field.field.paragraph.section_details.field_start_date',
    'field.field.paragraph.section_details.field_report_visibility',
    'field.field.paragraph.section_details.field_end_date',
    'field.field.node.quiz_assignment.field_quiz_assignment_section_de',
    'field.storage.paragraph.field_quiz_section',
    'field.storage.node.field_quiz_assignment_subject',
    'field.storage.node.field_quiz_assignment_school',
    'field.storage.node.field_quiz_assignment_quiz_type',
    'field.storage.node.field_quiz_assignment_class',
    'field.storage.node.field_quiz_assignment_chapter',
    'field.field.node.quiz_assignment.field_quiz_assignment_chapter',
    'field.field.node.quiz_assignment.field_quiz_assignment_class',
    'field.field.node.quiz_assignment.field_quiz_assignment_quiz_type',
    'taxonomy.vocabulary.school',
    'field.field.node.quiz_assignment.field_quiz_assignment_school',
    'taxonomy.vocabulary.section',
    'field.field.paragraph.section_details.field_quiz_section',
    'core.entity_view_display.paragraph.section_details.default',
    'core.entity_form_display.paragraph.section_details.default',
    'field.field.node.quiz_assignment.field_quiz_assignment_subject',
    'field.field.node.quiz_assignment.body',
    'core.entity_form_display.node.quiz_assignment.default',
    'core.entity_view_display.node.quiz_assignment.teaser',
    'core.entity_view_display.node.quiz_assignment.default'

  ];
  $call = \Drupal::configFactory();

  foreach ($configuration as $config) {
    // Deletes configuration.
    $call->getEditable($config)->delete();
  }
  
  //Deletes Vocabularies
  $vocabulary = ['board_field', 'chapter_field', 'class_field', 'cognitive_table_field',
    'concept_field', 'learning_objective', 'quiz_type', 'subject', 'text_book',
    'section',
  ];
  foreach ($vocabulary as $value) {
    $result = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $value)
      ->execute();
    entity_delete_multiple('taxonomy_term', $result);

  }

  // Deletes node of content type of question.
  $nodes = \Drupal::entityTypeManager()
    ->getStorage('node')
    ->loadByProperties(['type' => 'question']);

  foreach ($nodes as $node) {
    $node->delete();
  }

  // Deletes node of content type of quiz.
  $quiz_nodes = \Drupal::entityTypeManager()
    ->getStorage('node')
    ->loadByProperties(['type' => 'quiz']);

  foreach ($quiz_nodes as $node) {
    $node->delete();
  }

}// end of quiz_uninstall() function

/**
 * Creates vocabulary terms.
 */
function vocabulary_term($categories_vocabulary, $categories) {
  foreach ($categories as $category) {
    $term = Term::create([
      'parent' => [],
      'name' => $category,
      'vid' => $categories_vocabulary,
    ])->save();
  }
}

/**
 * Creates question.
 */
function create_question($field_option_value, $pos, $quiz_title, $term_name, $question) {
  $p_id = [];
  $field_correct = ['true', 'false'];

  // Creates paragraph of type quiz_question_options.
  for ($i = 0; $i < 4; $i++) {
    if ($i == $pos) {
      $value = TRUE;
    }
    else {
      $value = FALSE;
    }
    // Creates of paragraph type quiz_question_options.
    $paragraph = Paragraph::create([
      'type' => 'quiz_question_options',
      'field_correct' => $value,
      'field_ld_description' => 'option',
      'field_option_value' => $field_option_value[$i],
    ]);
    $paragraph->save();
    $p_id[] = $paragraph->id();
  }

  // Creates content of type question.
  $node = Node::create([
    'type' => 'question',
    'title' => $question[0],
    'body' => $question[1],
    'field_question_options' => Paragraph::loadMultiple($p_id),

  ]);
  $node->save();

  // Getting term of vocabulary in entity reference field of Quiz.
  foreach ($term_name as $name) {
    $term[] = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $name]);
  }

  // Creates content of type quiz.
  $quiz_node = Node::create([
    'type' => 'quiz',
    'title' => $quiz_title,
    'field_quiz_board_field' => $term[0],
    'field_quiz_chapter_field' => $term[1],
    'field_quiz_class_field' => $term[2],
    'field_quiz_duration' => 30,
    'field_quiz_subject' => $term[3],
    'field_quiz_question' => Node::load($node->id()),
  ]);
  $quiz_node->save();

  // Creates paragraph of type quiz_question_para.
  $paragraph = Paragraph::create([
    'type' => 'quiz_question_para',
    'field_question' => Node::load($node->id()),
  ]);
  $paragraph->save();
}
